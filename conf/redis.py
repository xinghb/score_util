redis_config = {
    "user": {
        "host" : "127.0.0.1", #数值类型：字符串
        "port" : 6379, #数值类型：整型
        "db" : 2, #选择使用的database, 数值类型：整型
        "retry" : 3, #重试次数,数值类型：整型
    },
    "ugc" : {
        "host" : "127.0.0.1",
        "port" : 6379,
        "db" : 2,
        "retry" : 3,
    },
    'rpc_host':{
        "host" : "192.168.191.13",
        "port" : 6378,
        "db" : 2,
        "retry" : 3
    },
    'monitor':{
        "host" : "192.168.191.13",
        "port" : 6378,
        "db" : 5,
        "retry" : 3
    }
}