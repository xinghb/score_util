import hashlib
import logging
import json
from datetime import datetime, timedelta

import ujson
from flask import session as sess, make_response, send_file

from flask import Flask, request, redirect, send_from_directory, render_template, abort,g
from flask_login import LoginManager, login_user, login_required, logout_user, current_user

from conf.mysql import mysql_config
from conf.redis import redis_config
from db.mysql_conn import MysqlPool
from models.auth_do import UtilAdmin
import settings
from solid import init_log
from solid.redis import getRedisConn
from utils.md5_util import setMd5

init_log()

app = Flask(__name__)
app.secret_key = settings.SECRET
app.debug = settings.DEBUG

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.refresh_view = 'login'



@login_manager.user_loader
def load_user(admin_id):
    return UtilAdmin.get_or_none(UtilAdmin.id==admin_id)


@app.route("/main/")
def main():
    session_id = sess.get("user_id")
    if session_id == None:
        return '-1'
    else:
        return "true"


@app.route('/', methods=['GET', 'POST'])
@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        account_name = request.form.get('account')
        password = request.form.get('password')
        admin = UtilAdmin.get_or_none(UtilAdmin.username==account_name)
        if admin:
            if admin.password == admin.passed(password):
                if not admin.active:
                    return render_template('login.html', messages='账户未激活，请联系管理员激活')
                login_user(admin, remember=True)
                app.permanent_session_lifetime = timedelta(hours=1)
                logging.info('登陆成功')
                return redirect(f'/index?{settings.VERSION}')
            else:
                return render_template('login.html', messages='账号或密码错误，请重新登陆')
        else:
            return render_template('login.html', messages='账号未存在，请注册以后登陆')



@app.route('/logout/')
def logout():
    logout_user()
    return redirect('/login/')


@app.route('/index/')
@login_required
def index():
    return app.send_static_file('html/index.html')

@app.route('/create_admin', methods=['GET', 'POST'])
def reg():
    arg =  request.args.to_dict()
    name = arg.get('name')
    pwd = arg.get('pwd')
    UtilAdmin.create(username=name,password=setMd5(setMd5(pwd)))
    return redirect('/login')

@app.route('/user_info',methods=['POST'])
@login_required
def get_user_info():
    ret = {
        'data':'',
        'status':'error'
    }
    r = getRedisConn(redis_config['user'])
    conn = MysqlPool(mysql_config.get('user_r'))
    data = ujson.loads(request.data)
    open_id = data['open_id']
    # 测试
    # r.set(f'API_SCORE:{open_id}', 1000)
    score = r.get(f'API_SCORE:{open_id}')
    # sql = "select u.user_name,u.phone,c.company_name from api_users as u,api_user_profile as c where u.open_id=c.open_id and u.open_id='{0}'".format(open_id)
    sql = "select api_users.user_name, api_users.phone, api_user_profile.company_name from api_users inner join api_user_profile where api_users.open_id='{0}'".format(open_id)

    result = conn.getOne(sql)
    print(result)
    if result:
        ret['data'] = result
        ret['status'] = 'success'
        ret['data']['score'] = score
        print(ret)
        return ujson.dumps(ret)
    return ujson.dumps(ret)
@app.route("/update_score",methods=['POST'])
@login_required
def update_score():
    ret = {
        'score': '',
        'status': 'error'
    }
    r = getRedisConn(redis_config['user'])
    data = ujson.loads(request.data)
    open_id = data['open_id']
    new_score = float(data['score'])
    old_score = r.get(f'API_SCORE:{open_id}')
    if old_score:
        if float(old_score)>0:
            new_score += float(old_score)
        r.set(f'API_SCORE:{open_id}', new_score)
        ret['score'] = new_score
        ret['status'] = 'success'

    print('updata_score',ret)
    return ujson.dumps(ret)

if __name__ == '__main__':
    app.run()
