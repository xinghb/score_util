
from solid.redis import getRedisConn
from conf.redis import redis_config
import json
def post_add_score(yc_id:str,score:float):
    r = getRedisConn(redis_config['user'])
    user_info = json.loads(r.get(f'api_score{yc_id}'))
    if user_info.get('util_admin',0) > 0:
        score += float(user_info.get('util_admin',0))
    user_info['util_admin'] = score
    r.set(f'{yc_id}',json.dumps(user_info))
    return {'message':'修改成功','status':'success'}

def get_user_list():
    pass

