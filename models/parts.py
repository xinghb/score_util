from utils.md5_util import setMd5
from .base import BaseModel
import datetime
from peewee import CharField, IntegerField, SQL, ForeignKeyField, FloatField, PrimaryKeyField, DateTimeField, \
    BooleanField, SmallIntegerField


class Parts(BaseModel):

    STATUS={
        0:'待绑定',
        1: '待审核',
        2: '审核通过',
        3: '保存',
        4: '审核不通过',
    }
    user_id = IntegerField(default=0,null=False,index=True,verbose_name='用户ID',help_text='用户ID')
    yc_id = CharField(max_length=60,default='',null=False,verbose_name='yc_id',help_text='yc_id')
    article_brand = CharField(max_length=60,default='',verbose_name='品牌名称',help_text='品牌名称')
    category_name = CharField(max_length=60,default='',null=False,verbose_name='产品类型',help_text='产品类型')
    code = CharField(max_length=30,default='',index=True,verbose_name='编码',help_text='编码')
    outer_code = CharField(max_length=30,default='',verbose_name='外部编码',help_text='外部编码')
    other_code = CharField(max_length=30,default='',verbose_name='其他编码',help_text='其他编码')

    adaptation = CharField(max_length=16,default='oe',verbose_name='关联类型 oe、model',help_text='适配类型')
    is_relation = BooleanField(default=False,verbose_name='是否已关联产品',help_text='是否已关联产品')
    brand_names = CharField(max_length=32,default='',verbose_name='适配品牌',help_text='适配品牌')
    place_origin = CharField(max_length=32,default='',verbose_name='产地',help_text='产地')
    produce_state = CharField(max_length=32,default='',verbose_name='生产状态',help_text='生产状态')
    price = CharField(max_length=30,default='',verbose_name='指导价',help_text='指导价')
    character = CharField(max_length=2048,default='',verbose_name='产品特点(json)',help_text='产品特点(json)')
    product_params = CharField(max_length=2048,default='',verbose_name='产品参数(json)',help_text='产品参数(json)')
    images = CharField(max_length=128,default='',verbose_name='封面图',help_text='封面图')
    images_count= IntegerField(default=0,verbose_name='图片数量',help_text='图片数量')
    import_models = CharField(max_length=1024,default='',verbose_name='导入车型',help_text='导入车型')
    status = SmallIntegerField(default=0,index=True,verbose_name='审核状态',help_text='审核状态')
    remark = CharField(max_length=128,default='',verbose_name='审核意见',help_text='审核意见')
    #is_delete_time = IntegerField(default=0, verbose_name='是否已删除（0 未删除, 大于0则是删除时间）',help_text='是否已删除（0 未删除, 大于0则是删除时间）')

    #图片、产品编码、OE、产品类型、产地、生产状态、审核、审核备注
    class Meta:
        table_name = 'parts'
        verbose_name= '产品列表'

    def _to_dict(self):
        keys=['show_id','article_brand','category_name','images','code','place_origin','produce_state','status','remark']
        return self.to_dict(keys)