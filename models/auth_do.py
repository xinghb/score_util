#!/usr/bin/env python
# encoding: utf-8
'''
@author: qiuyan
@contact: winston@peipeiyun.com
@file: auth_do.py
@time: 2019/1/14 5:04 PM
@desc:
'''
from utils.md5_util import setMd5
from .base import BaseModel
import datetime
from peewee import CharField,IntegerField,SQL,ForeignKeyField,FloatField,PrimaryKeyField,DateTimeField,BooleanField, BigIntegerField

class ApiUsers(BaseModel):

    class Meta:
        table_name = 'api_users'

    user_name = CharField(max_length=16,verbose_name='用户名',default="")
    passwd = CharField( max_length=32, verbose_name='密码',default="")
    phone = CharField(max_length=16, verbose_name='手机信息',default='')
    open_id = CharField(max_length=32,verbose_name='用户的Key',default='')
    secret_key = CharField(default="", max_length=50,verbose_name='密钥')
    score = BigIntegerField(default=0.0, verbose_name='总积分')
    user_img = CharField(default="", max_length=64,verbose_name='用户头像')
    auther_status = IntegerField(default=0 ,verbose_name='认证状态')
    validate_time = DateTimeField(verbose_name='到期时间',default=datetime.datetime.now)
    total_charge = IntegerField(default=0, verbose_name='总支付金额')
    status = BooleanField(default=True ,verbose_name='用户状态，1，0表示不可用')
    follow = CharField(max_length=64,verbose_name='跟进人',default="")
    last_pay_time = DateTimeField(verbose_name='最新支付时间',default=datetime.datetime.now)
    free_charge = IntegerField(default=0, verbose_name='赠送金额')

    def to_dict(self):
        pass

class ApiUserProfile(BaseModel):

    class Meta:
        table_name = 'api_user_profile'

    id = PrimaryKeyField()
    username = CharField(null=True)
    company_name = CharField(null=True)
    open_id = CharField(max_length=64,verbose_name='用户id')
    company_address = CharField(null=True)
    company_licenss = CharField(constraints=[SQL("DEFAULT '0'")], null=True)
    company_licenss_url = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    company_logo_url = CharField(null=True)
    company_user = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    company_phone = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    company_tax = CharField(column_name='company_tax_id', constraints=[SQL("DEFAULT ''")], null=True)
    wx_id = CharField(null=True)
    wx_nikename = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    wx_qrcode = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    user_id = IntegerField(verbose_name='', null=False)

    def to_dict(self):
        return {
            "username": self.username, 
            "company_name": self.company_name, 
            "company_address": self.company_address, 
            "company_licenss": self.company_licenss,
            "company_licenss_url": self.company_licenss_url,
            "company_logo_url": self.company_logo_url,
            "company_user": self.company_user,
            "company_phone": self.company_phone,
            "company_tax": self.company_tax,
            "wx_id": self.wx_id,
            "wx_nikename": self.wx_nikename,
            "wx_qrcode": self.wx_qrcode
        }

class ApiWhiteIp(BaseModel):

    class Meta:
        table_name='api_white_ip'

    id = PrimaryKeyField()
    open_id = CharField(max_length=32,verbose_name='用户ID')
    white_list = CharField(max_length=50,verbose_name='白名单')
    user = ForeignKeyField(ApiUsers, backref='apiwhiteip', on_delete="CASCADE")

    def to_dict(self):
        return {"id": self.id, "open_id": self.open_id, "white_list": self.white_list}

'''
class ApiPermissionGroup(BaseModel):
    class Meta:
        table_name='api_permission_group'

    name = CharField(max_length=30,verbose_name='名称')
    permisions = CharField(max_length=510,verbose_name='权限集合')


class UserPermision(BaseModel):

    class Meta:
        table_name = 'user_permision'

    open_id = CharField(max_length=30,verbose_name='用户ID')
    brandCode_list = CharField(max_length=512,verbose_name='有权限的品牌')
    permisions = CharField(max_length=1024,verbose_name='所有的用户权限')
'''


class UtilAdmin(BaseModel):
    '''
    工具后台账户
    '''
    class Meta:
        table_name='util_admin'

    username = CharField(max_length=16, verbose_name='用户名', default="")
    password = CharField(max_length=32, verbose_name='密码', default="")
    active = BooleanField(default=True,verbose_name='激活状态')


    def passed(self, password):
        '''
        生成密码
        :param password:
        :return:
        '''
        return setMd5(setMd5(password))

    def set_pwd(self, password):
        '''
        生成密码
        :param password:
        :return:
        '''
        return setMd5(setMd5(password))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def to_dict(self):
        return {"id": self.id, "username": self.username, "white_list": self.white_list}
