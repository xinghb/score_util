from conf.mysql import mysql_config
from models.auth_do import UtilAdmin, ApiUsers, ApiUserProfile
from models.base import db, RetryMySQLDatabase

user = mysql_config['ssh_database']

ssh_db=RetryMySQLDatabase(user.get('db'), user=user.get('username'), password=user.get('pwd'),host=user.get('host'), port=user.get('port'))

if __name__ == "__main__":
    db.connect()
    UtilAdmin.create_table()
    db.commit()
    db.close()
    ssh_db.connect()
    ApiUsers.create_table()
    ApiUserProfile.create_table()
    ssh_db.commit()
    ssh_db.close()