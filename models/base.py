#!/usr/bin/env python
# encoding: utf-8
'''
@author: qiuyan
@contact: winston@peipeiyun.com
@file: base.py
@time: 2019/1/8 10:13 AM
@desc:
'''
from peewee import Model,OperationalError,DateTimeField,SmallIntegerField,MySQLDatabase,SqliteDatabase,BooleanField,CharField
import datetime
from conf.mysql import mysql_config
import random

user = mysql_config['user_r']
from peewee import __exception_wrapper__

class RetryOperationalError(object):

    def execute_sql(self, sql, params=None, commit=True):
        try:
            cursor = super(RetryOperationalError, self).execute_sql(
                sql, params, commit)
        except OperationalError:
            if not self.is_closed():
                self.close()
            with __exception_wrapper__:
                cursor = self.cursor()
                cursor.execute(sql, params or ())
                if commit and not self.in_transaction():
                    self.commit()
        return cursor


class RetryMySQLDatabase(RetryOperationalError,MySQLDatabase):#, peewee_async.PooledMySQLDatabase
    pass

db=RetryMySQLDatabase(user.get('db'), user=user.get('username'), password=user.get('pwd'),host=user.get('host'), port=user.get('port'))

class BaseModel(Model):
    class Meta:
        database = db

    show_id = CharField(max_length=16,default='',verbose_name='show_id')
    update_time = DateTimeField(default=datetime.datetime.now())
    create_time = DateTimeField(default=datetime.datetime.now(), index=True)
    deleted = BooleanField(default=0)

    @classmethod
    def get(cls, *query, **filters):
        try:
            sq = cls.select()
            if query:
                sq = sq.where(*query)
            if filters:
                sq = sq.filter(**filters)
            return sq.get()
        except:
            # logging.error(traceback.format_exc())
            return None

    def get_id_by_show_id(self, showId):
        '''
        根据showid获取真实id值
        :param showId: type string 自动增长列id值
        :return:
        '''
        i = showId[len(showId) - 1]
        j = showId[0:len(showId) - 1]
        k = (int(j) - int(i) - 10000) / int(i)
        return k

    def set_show_id_by_id(self, id):
        '''
        设置showid
        :param id: 真实id
        :return:
        '''
        a = random.randint(1, 9)
        b = a * int(id) + 10000 + a
        j = str(b) + str(a)
        return j
