import os

import pandas as pd
import paypalrestsdk as paypalrestsdk
import ujson

from models.parts import Parts
from settings import PAYPAL_PAY
import zipfile

import shutil


def read_exc():
    df = pd.read_excel('/Users/gary/Documents/user_error_log.xlsx')  # 这个会直接默认读取到这个Excel的第一个表单
    data = df.iloc[:, 4].values
    for d in data:
        if d != 'params':
            vin = ujson.loads(d).get('vin')
            print(vin)


def create():
    paypalrestsdk.configure({
        'mode': 'sandbox',  # sandbox or live
        'client_id': PAYPAL_PAY.get("test").get('client'),
        'client_secret': PAYPAL_PAY.get("test").get('secret')
    })
    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": 'paypal'
        },
        "redirect_urls": {
            "return_url": 'http://test.007vin.com/pays/paypal_call',
            "cancel_url": 'http://www.baidu.com'
        },
        "transactions": [{
            "amount": {
                "total": str(10),
                "currency": "USD"
            }
        }]
    })

    code = 1
    if payment.create():
        for link in payment.links:
            if link.method == "REDIRECT":
                # Capture redirect url
                redirect_url = link.href
                payment_id = payment.id
                return payment_id, code, redirect_url
    else:
        code = 0
        print('')


class Node:

    def __init__(self, value, next=None):
        self.value = value
        self.next = next

def sort_w_check(seq):
    n = len(seq)
    for i in range(n-1):
        print(i)
        print(seq[i])
        print(seq[i + 1])
        if seq[i] > seq[i+1]:
            break
        else:
           pass

if __name__ == '__main__':
    # L = Node("a",Node("b",Node("c",Node("d"))))
    # print(L.next.next.value)
    # print(L.value)
    # seq1 = [[0, 1], [2], [3, 4, 5]]
    # s = 0
    # for seq2 in seq1:
    #     for x in seq2:
    #         s += x
    #         print(s)

    sort_w_check([1,4,3,2,0])