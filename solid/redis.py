import redis
import traceback
import time
import logging

def getRedisConn(config_dict):

    return setRedisConn(config_dict["host"], config_dict["port"], config_dict["db"], config_dict["retry"])

def setRedisConn(host="127.0.0.1", port=6379, db=1, retry=3, timeout=10):
    """
    :链接redis
    @host:主机
    @port:端口
    @timeout:超时秒数
    @db:database
    @retry:重试次数
    """
    r=None
    i=0

    while i<retry:
        try:
            pool = redis.ConnectionPool(host=host, port=port, db=db, decode_responses=True)
            r = redis.Redis(connection_pool=pool, decode_responses=True)
            if not r:
                logging.info("第[%d]连接失败，继续"%i)
            else:
                break
        except:
            logging.error(traceback.format_exc())
            time.sleep(1)
        i+=1
    return r